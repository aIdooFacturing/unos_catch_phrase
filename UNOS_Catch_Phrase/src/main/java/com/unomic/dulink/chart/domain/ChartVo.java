
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String msg;
	String rgb;
	
	//common
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String name;
	
}
