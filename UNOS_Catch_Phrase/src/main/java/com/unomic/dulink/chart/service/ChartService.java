package com.unomic.dulink.chart.service;

import com.unomic.dulink.chart.domain.ChartVo;


public interface ChartService {
	public void addBanner(ChartVo chartVo) throws Exception;
	
	//common func
	public String login(ChartVo chartVo) throws Exception;
	public String getStartTime(ChartVo chartVo) throws Exception;
	public String getComName(ChartVo chartVo) throws Exception;
	public ChartVo getBanner(ChartVo chartVo) throws Exception;
	
};
